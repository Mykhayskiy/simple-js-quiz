LICENSE GNU General Public License v3.0
README.md

This project has been created just for fun and to learn how to use the JSON file as a database. 
It is a simple program that allows you to create a quiz based on JSON file and then play it.
JSON file gives possibility to create a quiz with any number of questions and answers.
You can add as many questions as you want, but remember that the more questions you add, the more time it takes to create a quiz.
Each question has a title, background image, main image, and answers.
Also, each answer can be changed and has a score that is added to the total score when the answer is selected.

You can easily change the questions storage from JSON file to any other database or API.
Just follow the JSON file structure and you will be able to use external API as a storage.

## How to use
To create a quiz you need to create a JSON file with the following structure:
```json
{
  "title": "Do you like ponies?",
  "mainImage": "/images/pinke_pie.webp",
  "backgroundImage": "/images/pinkie_pie_bedroom.jpeg",
  "body": "Ponie is a small horse. They are very cute and funny. Do you like them?",
  "buttons": [
    {
      "id": "yesButton",
      "buttonName": "Yes",
      "value": 3
    },
    {
      "id": "noButton",
      "buttonName": "No",
      "value": 2
    },
    {
      "id": "uncertainButton",
      "buttonName": "Not sure",
      "value": 0
    }
  ]
}
```
JSON file must be placed in the gameData folder and must have the .json extension.
images folder contains images that are used in the quiz.

Then you need to run the index.html file and that's it.