const startBtn = document.getElementById('start');
const stopBtn = document.getElementById('stop');
const gameRulesBlock = document.getElementById('gameRules');
const gameBlock = document.getElementById('game');

function checkSessionStorage() {
    if (sessionStorage.getItem('userName') == null) {
        gameRulesBlock.removeAttribute('hidden');
        gameBlock.setAttribute('hidden', 'hidden');
    } else {
        gameRulesBlock.setAttribute('hidden', 'hidden');
        gameBlock.removeAttribute('hidden');
        document.getElementById('currentPlayerName').innerHTML = sessionStorage.getItem('userName');
        document.getElementById('currentPlayerScore').innerHTML = sessionStorage.getItem('userScore');
        document.getElementById('currentGameStep').innerHTML = sessionStorage.getItem('currentGameStep');

        let questionNumber = parseInt(sessionStorage.getItem('currentQuestion'));

        if (questionNumber >= parseInt(sessionStorage.getItem('totalQuestions'))) {
            document.getElementById('currentGameStep').innerHTML = "-";
            document.getElementById('totalScore').innerHTML = sessionStorage.getItem('userScore');
        } else {
            gameQuestions(questionNumber)
                .then(result => {
                    buildQuestionBlock(result);
                })
                .catch(error => {
                    console.error('Error fetching JSON:', error);
                    throw error;
                });
        }
    }
}

//Run each time page is loaded for F5 or browser refresh purposes
//Should be optimized
checkSessionStorage();


function initializeStorage(userName) {
    sessionStorage.setItem('userName', userName);
    sessionStorage.setItem('userScore', 0);
    sessionStorage.setItem('currentQuestion', 0);
    sessionStorage.setItem('currentGameStep', 1);
}

function buildQuestionBlock(buildData) {
    let gameBlock = document.getElementById('gameBlock');
    let stepMainImage = document.getElementById('stepMainImage');

    document.getElementById('stepTitle').innerHTML = buildData.title ? buildData.title : '';
    document.getElementById('stepBody').innerHTML = buildData.body ? buildData.body : '';
    let stepActions = document.getElementById('stepActions');

    if (buildData.mainImage) {
        stepMainImage.innerHTML = '<img src="' + buildData.mainImage + '" alt="">';
    }

    if (buildData.backgroundImage) {
        gameBlock.style.backgroundImage = 'url(' + buildData.backgroundImage + ')';
    }

    if (buildData.buttons) {
        let buttons = buildData.buttons;
        const dynamicButtons = {};

        for (let i = 0; i < buttons.length; i++) {
            let buttonVarName = buttons[i].id;

            dynamicButtons[buttonVarName] = document.createElement('button');
            dynamicButtons[buttonVarName].setAttribute('id', buttonVarName);
            dynamicButtons[buttonVarName].id = buttonVarName;
            dynamicButtons[buttonVarName].value = buttons[i].value;
            dynamicButtons[buttonVarName].innerHTML = buttons[i].buttonName;
            stepActions.appendChild(dynamicButtons[buttonVarName]);

            let handler = buttonVarName + 'Click';

            dynamicButtons[buttonVarName].addEventListener('click', function (e) {
                window[handler](e, this.value);
            });
        }
    }
}

//For this implementation we created separate functions for each button
//But in this case it is not necessary, we can use one function for all buttons
function yesButtonClick(e, value) {
    e.preventDefault();
    updateScore(parseInt(value));
}

function noButtonClick(e, value) {
    e.preventDefault();
    updateScore(parseInt(value));
}

function uncertainButtonClick(e, value) {
    e.preventDefault();
    updateScore(parseInt(value));
}

function updateScore(value) {
    let currentScore = parseInt(sessionStorage.getItem('userScore'));
    sessionStorage.setItem('userScore', currentScore + value);
    let currentQuestion = parseInt(sessionStorage.getItem('currentQuestion'));
    sessionStorage.setItem('currentQuestion', currentQuestion + 1);
    let currentGameStep = parseInt(sessionStorage.getItem('currentGameStep'));
    sessionStorage.setItem('currentGameStep', currentGameStep + 1);
    window.location.reload();
}

startBtn.addEventListener('click', function (e) {
    e.preventDefault();
    let userName = document.getElementById('userName').value;
    if (userName == '') {
        alert('Please enter your name!');
        return false;
    }
    initializeStorage(userName);
    window.location.reload();
});

stopBtn.addEventListener('click', function (e) {
    e.preventDefault();
    sessionStorage.clear();
    checkSessionStorage();
});


function gameQuestions(num) {
    return fetch('gameData/data.json')
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            const title = data[num].title;
            const body = data[num].body;
            const buttons = data[num].buttons;
            const mainImage = data[num].mainImage;
            const backgroundImage = data[num].backgroundImage;
            sessionStorage.setItem('totalQuestions', data.length);

            return {title, body, buttons, mainImage, backgroundImage};
        })
        .catch(error => {
            console.error('Error fetching JSON:', error);
            throw error;
        });
}
